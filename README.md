# Introduction to Graphic Design Talks on Linux with GNOME

Welcome, everyone! It's a pleasure to have you here for this series of talks on graphic design using Linux as the operating system, GNOME as the desktop environment, and free software tools such as Blender, Inkscape, and GIMP. Let's dive into a world where creativity meets open and collaborative technology.

## Why Linux and GNOME?
**Linux**, known for its stability, security, and flexibility, has become an increasingly popular choice among graphic designers and creatives worldwide. Using Linux allows us to customise our work environment according to our specific needs and to take advantage of the wide range of available free software tools.

**GNOME**, one of the most popular desktop environments for Linux, offers a clean, modern, and highly intuitive interface. With GNOME, users enjoy an efficient and elegant user experience, allowing us to focus on our creative tasks without distractions.

## GNOME Projects

- [**GNOME Engagement Team**](https://gitlab.gnome.org/Teams/Engagement) The Engagement Team works to promote GNOME by helping our communications with users, developers, contributors, partners and anyone else who might be interested in the project.

- [**GNOME Latam**](https://floss.social/@GNOMELatam) The GNOME Latam project is a group of GNOME members. This project specifically targets people in Latin America who are interested and motivated to participate, whether or not they have technical knowledge. 

- [**GNOME Chile**](https://noticias.gnome.cl/) The GNOME Chile project is a non-profit organization founded in late 2001. It was established with the clear goal of reaching the entire IT community in the country, with the intention of promoting the use, distribution, and development of free software, especially in projects related to the GNOME environment.

- [**GNOME Handibox**](https://gitlab.gnome.org/fabioduran/handibox) Handibox is an accessibility tool that will allow Human-Computer interaction with people who have some degree of motor disability. Through the Handibox, people will have the possibility to carry out elementary activities.

Benefits of Free Software
Choosing Linux and free software tools is not just an economic decision but also a commitment to freedom and collaboration. The global community of users and developers behind these projects is vibrant and always willing to share knowledge and offer support. Additionally, using Linux ensures greater security and customisation in our work tools.

Conclusion
In these talks, our goal is to provide you with the knowledge and skills necessary to make the most of these graphic design tools on Linux. Whether you are a beginner just starting out or an experienced designer looking for new and powerful alternatives, here you will find resources and support for your creative projects.

I hope you enjoy this series of talks and that together we can discover everything that graphic design on Linux with GNOME has to offer!

This introduction sets the right context for your talks, highlighting the benefits of using Linux and GNOME, as well as the capabilities of Blender, Inkscape, and GIMP. I hope you find it useful!

## GNOME Latam 2024 - Como ser parte de GNOME Engagement Team

![GNOME_Latam_2024](/uploads/a03d8f85b07fcaf1a9578e35926316f9/GNOME_Latam_2024.png)

[Como ser parte de GNOME Engagement Team - Video](https://youtu.be/Gk6bkDcAVto?si=lYkIlRFJ0BdgmppT)

## GNOME Latam 2023 - Como colaborar con GNOME como Diseñador Visual

![GNOME_Latam_2023](/uploads/aaac903a77c27334af7cb6987a7634bc/GNOME_Latam_2023.png)

[Cómo_colaborar_con_GNOME_como_diseñador_visual.pdf](/uploads/7858e264bc9d5273a41c05d06abef761/Cómo_colaborar_con_GNOME_como_diseñador_visual.pdf)

## GNOME Asia 2023 - How to collaborate with GNOME as a Visual Designer

![GNOME_Asia_2023](/uploads/be0b81bac55e8540f923495fe72fc05c/GNOME_Asia_2023.png)

[How_to_collaborate_with_GNOME_as_a_Visual_Designer.pdf](/uploads/83494485fe465647eee047516f186024/How_to_collaborate_with_GNOME_as_a_Visual_Designer.pdf)

## GUADEC 2022 - How to be a Graphic Designer for GNOME Engagement Team

![GUADEC_2022](/uploads/5154ce9d491c6ff996fcec011176186e/GUADEC_2022.png)

[How_to_be_a_Graphic_Designer_for_GNOME_Engagement_Team.pdf](/uploads/9376383eec1650100550221a5ecd36b8/How_to_be_a_Graphic_Designer_for_GNOME_Engagement_Team.pdf)

## GNOME Asia 2022 - How to collaborate with GNOME as a Visual Designer

![GNOME_Asia_2022](/uploads/00e448df02196221bb9bcdfc5a2c0294/GNOME_Asia_2022.png)

[How_to_collaborate_with_GNOME_as_a_Visual_Designer.pdf](/uploads/2d599134f79f6001a16321368f0d06ac/How_to_collaborate_with_GNOME_as_a_Visual_Designer.pdf)

## GNOME Asia 2021 - Design in GNOME with Engagement Team

![GNOME_Asia_2021](/uploads/8d7efa54093cdd509ff5defaa5118af3/GNOME_Asia_2021.png)

[Design_with_GNOME_Engagement_Team.pdf](/uploads/8075a3434e810ea601e70691df569f0d/Design_with_GNOME_Engagement_Team.pdf)